# Template

This is a template for java projects:
* it has code coverage 100%
* it has a code formatter
* it has a gitlab CI setup

## Tech
- JUnit 4
- gradle 5.5.1
- Jacco
- Java 12
- Google formatter
- Coverage CI reporter
